import request from '@/utils/request'

// 查询APP信息列表
export function getAppType(query) {
    return request({
      url: '/app/type/list',
      method: 'get',
      params: query
    })
  }