import request from '@/utils/request'

// 查询APP信息列表
export function listInfo(query) {
    return request({
      url: '/app/info/list',
      method: 'get',
      params: query
    })
  }


// 新增应用数据
export function addAppInfo(data) {
  return request({
    url: '/app/info',
    method: 'post',
    data: data
  })
}

// 删除应用数据
export function delAppInfo(ids) {
  return request({
    url: '/app/info/' + ids,
    method: 'delete'
  })
}


// 查询应用数据
export function getAppInfo(id) {
  return request({
    url: '/app/info/' + id,
    method: 'get'
  })
}

export function getAppDetails(id) {
  return request({
    url: '/app/info/details/' + id,
    method: 'get'
  })
}




// 更新应用信息
export function updateAppInfo(data) {
  return request({
    url: '/app/info/',
    method: 'put',
    data: data
  })
}

 
  