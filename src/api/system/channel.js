import request from '@/utils/request'
import { praseStrEmpty } from "@/utils/ruoyi";

// 查询用户列表
export function listChannel(query) {
    return request({
      url: '/system/channel/list',
      method: 'get',
      params: query
    })
  }
  
  // 查询用户详细
  export function getChannel(id) {
    return request({
      url: '/system/channel/' + id,
      method: 'get'
    })
  }
  
  // 新增用户
  export function addChannel(data) {
    return request({
      url: '/system/channel',
      method: 'post',
      data: data
    })
  }
  
  // 修改用户
  export function updateChannel(data) {
    return request({
      url: '/system/channel',
      method: 'put',
      data: data
    })
  }
  
  // 删除用户
  export function delChannel(id) {
    return request({
      url: '/system/channel/' + id,
      method: 'delete'
    })
  }
  
  // 导出用户
  export function exportUser(query) {
    return request({
      url: '/system/user/export',
      method: 'get',
      params: query
    })
  }
  
  
  // 下载用户导入模板
  export function importTemplate() {
    return request({
      url: '/system/user/importTemplate',
      method: 'get'
    })
  }