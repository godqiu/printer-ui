import { constantRoutes } from '@/router'
import { getRouters } from '@/api/menu'
import Layout from '@/layout/index'
import ParentView from '@/components/ParentView';

const permission = {
  state: {
    routes: [],
    addRoutes: [],
    sidebarRouters: []
  },
  mutations: {
    SET_ROUTES: (state, routes) => {
      state.addRoutes = routes
      state.routes = constantRoutes.concat(routes)
    },
    SET_SIDEBAR_ROUTERS: (state, routers) => {
      state.sidebarRouters = constantRoutes.concat(routers)
    },
  },
  actions: {
    // 生成路由
    GenerateRoutes({ commit }) {
      return new Promise(resolve => {
        // 向后端请求路由数据
        getRouters().then(res => {
          var jsonString = "[\n" +
            "  {\n" +
            "    \"name\": \"System\",\n" +
            "    \"path\": \"/system\",\n" +
            "    \"hidden\": false,\n" +
            "    \"redirect\": \"noRedirect\",\n" +
            "    \"component\": \"Layout\",\n" +
            "    \"alwaysShow\": true,\n" +
            "    \"meta\": {\n" +
            "      \"title\": \"系统管理\",\n" +
            "      \"icon\": \"system\",\n" +
            "      \"noCache\": false\n" +
            "    },\n" +
            "    \"children\": [\n" +
            "      {\n" +
            "        \"name\": \"User\",\n" +
            "        \"path\": \"user\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/user/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"用户管理\",\n" +
            "          \"icon\": \"user\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Role\",\n" +
            "        \"path\": \"role\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/role/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"角色管理\",\n" +
            "          \"icon\": \"peoples\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Menu\",\n" +
            "        \"path\": \"menu\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/menu/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"菜单管理\",\n" +
            "          \"icon\": \"tree-table\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Dept\",\n" +
            "        \"path\": \"dept\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/dept/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"部门管理\",\n" +
            "          \"icon\": \"tree\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Post\",\n" +
            "        \"path\": \"post\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/post/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"岗位管理\",\n" +
            "          \"icon\": \"post\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Dict\",\n" +
            "        \"path\": \"dict\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/dict/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"字典管理\",\n" +
            "          \"icon\": \"dict\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Config\",\n" +
            "        \"path\": \"config\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/config/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"参数设置\",\n" +
            "          \"icon\": \"edit\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Notice\",\n" +
            "        \"path\": \"notice\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"system/notice/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"通知公告\",\n" +
            "          \"icon\": \"message\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Monitor\",\n" +
            "    \"path\": \"/monitor\",\n" +
            "    \"hidden\": false,\n" +
            "    \"redirect\": \"noRedirect\",\n" +
            "    \"component\": \"Layout\",\n" +
            "    \"alwaysShow\": true,\n" +
            "    \"meta\": {\n" +
            "      \"title\": \"系统监控\",\n" +
            "      \"icon\": \"monitor\",\n" +
            "      \"noCache\": false\n" +
            "    },\n" +
            "    \"children\": [\n" +
            "      {\n" +
            "        \"name\": \"Online\",\n" +
            "        \"path\": \"online\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"monitor/online/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"在线用户\",\n" +
            "          \"icon\": \"online\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Job\",\n" +
            "        \"path\": \"job\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"monitor/job/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"定时任务\",\n" +
            "          \"icon\": \"job\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Druid\",\n" +
            "        \"path\": \"druid\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"monitor/druid/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"数据监控\",\n" +
            "          \"icon\": \"druid\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Server\",\n" +
            "        \"path\": \"server\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"monitor/server/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"服务监控\",\n" +
            "          \"icon\": \"server\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Cache\",\n" +
            "        \"path\": \"cache\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"monitor/cache/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"缓存监控\",\n" +
            "          \"icon\": \"redis\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  },\n" +
            "  {\n" +
            "    \"name\": \"Tool\",\n" +
            "    \"path\": \"/tool\",\n" +
            "    \"hidden\": false,\n" +
            "    \"redirect\": \"noRedirect\",\n" +
            "    \"component\": \"Layout\",\n" +
            "    \"alwaysShow\": true,\n" +
            "    \"meta\": {\n" +
            "      \"title\": \"系统工具\",\n" +
            "      \"icon\": \"tool\",\n" +
            "      \"noCache\": false\n" +
            "    },\n" +
            "    \"children\": [\n" +
            "      {\n" +
            "        \"name\": \"Build\",\n" +
            "        \"path\": \"build\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"tool/build/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"表单构建\",\n" +
            "          \"icon\": \"build\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Gen\",\n" +
            "        \"path\": \"gen\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"tool/gen/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"代码生成\",\n" +
            "          \"icon\": \"code\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      },\n" +
            "      {\n" +
            "        \"name\": \"Swagger\",\n" +
            "        \"path\": \"swagger\",\n" +
            "        \"hidden\": false,\n" +
            "        \"component\": \"tool/swagger/index\",\n" +
            "        \"meta\": {\n" +
            "          \"title\": \"系统接口\",\n" +
            "          \"icon\": \"swagger\",\n" +
            "          \"noCache\": false\n" +
            "        }\n" +
            "      }\n" +
            "    ]\n" +
            "  }\n" +
            "]";
         /* const sdata = JSON.parse(JSON.stringify(res.data))
          const rdata = JSON.parse(JSON.stringify(res.data))*/

          const sdata = JSON.parse(jsonString)
          const rdata = JSON.parse(jsonString)
          console.log("permission------------------GenerateRoutes--------------sdata::" + JSON.stringify(res.data));
          const sidebarRoutes = filterAsyncRouter(sdata)
          const rewriteRoutes = filterAsyncRouter(rdata, false, true)
          rewriteRoutes.push({ path: '*', redirect: '/404', hidden: true })
          commit('SET_ROUTES', rewriteRoutes)
          commit('SET_SIDEBAR_ROUTERS', sidebarRoutes)
          resolve(rewriteRoutes)
        })
      })
    }
  }
}

// 遍历后台传来的路由字符串，转换为组件对象
function filterAsyncRouter(asyncRouterMap, lastRouter = false, type = false) {
  return asyncRouterMap.filter(route => {
    console.log("filterAsyncRouter----11111------------lastRouter::" + lastRouter + "---------------type::" + type);
    if (type && route.children) {
      console.log("filterAsyncRouter---22222-------------route.children::" + JSON.stringify(route.children) + "---------------type::" + type);
      route.children = filterChildren(route.children)
    }
    if (route.component) {
      // Layout ParentView 组件特殊处理
      console.log("filterAsyncRouter---33333-------------route.component::" + JSON.stringify(route.component) + "---------------type::" + type);
      if (route.component === 'Layout') {
        route.component = Layout
      } else if (route.component === 'ParentView') {
        route.component = ParentView
      } else {
        route.component = loadView(route.component)
      }
    }
    if (route.children != null && route.children && route.children.length) {
      console.log("filterAsyncRouter---44444-------------route.children::" + JSON.stringify(route.children) + "======route::" + route + "=====type::" + type);
      route.children = filterAsyncRouter(route.children, route, type)
    } else {
      delete route['children']
      delete route['redirect']
    }
    return true
  })
}

function filterChildren(childrenMap, lastRouter = false) {
  var children = [];
  console.log("filterChildren--555555-------------childrenMap::" + JSON.stringify(childrenMap) + "======lastRouter::" + lastRouter );
  childrenMap.forEach((el, index) => {
    if (el.children && el.children.length) {
      if (el.component === 'ParentView') {
        el.children.forEach(c => {
          c.path = el.path + '/' + c.path
          if (c.children && c.children.length) {
            children = children.concat(filterChildren(c.children, c))
            return
          }
          children.push(c)
        })
        return
      }
    }
    if (lastRouter) {
      el.path = lastRouter.path + '/' + el.path
    }
    children = children.concat(el)
  })
  console.log("filterChildren--66666-------------children::" + children);
  return children
}

export const loadView = (view) => { // 路由懒加载
  return (resolve) => require([`@/views/${view}`], resolve)
}

export default permission
